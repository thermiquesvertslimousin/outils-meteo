# Outils météo

## Cloner le projet
Protocol SSH :
```shell
git clone git@gitlab.com:thermiquesvertslimousin/outils-meteo.git
```

Protocol HTTPS :
```shell
git clone https://gitlab.com/thermiquesvertslimousin/outils-meteo.git
```

## Générer la documentation

### Prérequis

Les outils suivants sont à installer :
* [Ruby](https://www.ruby-lang.org/fr/)
* [asciidoctor](https://docs.asciidoctor.org/asciidoctor/latest/install/)
* [asciidoctor-pdf](https://docs.asciidoctor.org/pdf-converter/latest/install/)
* [rouge](https://rubygems.org/gems/rouge)

### PDF
```shell
asciidoctor-pdf -a compress -o output/Presentation_outils_meteo.pdf -b pdf src/main.adoc
```

### HTML
```shell
asciidoctor-pdf -a compress -o output/Presentation_outils_meteo.html -b html src/main.adoc
```
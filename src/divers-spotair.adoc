== Spotair

https://www.spotair.mobi/[Spotair] est un site permettant d'afficher les mesures des balises, les images provenant de webcams et les sites de vol sur une carte.

Un clic sur une icone affiche des informations dans le bandeau latéral à gauche de l'écran, si plusieurs icones sont regroupées au même endroit, les différents éléments sont affichés.

.Capture page Spotair avec les espaces aériens activés
image::Spotair-1.png[]

Un appui sur une information dans le bandeau latéral permet d'afficher plus d'informations (l'historique des balises, une image de webcam, un descriptif d'un site).

.Affichage de l'historique d'une balise
image::Spotair-historique.png[]

Dans le coin inférieur droit, des boutons permettent plusieurs réglages.

* Un bouton préférence : Réglage des unités des valeurs affichées, des seuils pour afficher des vitesses de vent trop (ou pas assez) importantes.

* Un bouton couches de carte : Réglage du fond de carte et du type de données à afficher (balises, webcam, sites de vol, espaces aériens, paraglidable, ...)

* Un bouton outils proposant :
** Hike and Fly : Un estimatif de la distance parcouru en finesse en partant d'un point donné. Un clic droit sur le bouton permet de régler la finesse à utiliser.
** Mesure : ajout d'un segment permettant de calculer des distances.

image:Spotair-preferences.png[Préférences, 200, 300] image:Spotair-couches.png[Couches, 200, 300] image:Spotair-outils.png[]

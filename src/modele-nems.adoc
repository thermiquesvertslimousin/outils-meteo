== NEMS
Modèle suisse (Météoblue), issue de la compilation de modèles classiques sur lesquels sont appliqués de l'intelligence artificielle.

Maillage de 4 km.

*Avantages* : une approche différente souvent pertinente.

*Inconvénients* : modèle beaucoup plus performant en montagne.
